const bodyParser = require('body-parser')
const {Router} = require('express')
const ResponseMiddleware = require('../middlewares/ResponseMiddleware');
const UserController = require('../controllers/api/UserController.js')

const api = Router();
const userController = new UserController();

api.use(bodyParser.json())
api.use(ResponseMiddleware())

api.get('/user',userController.getUser)
api.get('/user/:index',userController.getDetailUser)
api.post('/user',userController.insertUser)
api.put('/user/:index',userController.updateUser)
api.delete('/user/:index',userController.deleteUser)

module.exports = api