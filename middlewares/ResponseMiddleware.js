module.exports = function(){
    return function (req,res,next){
        res.serverError = function(){};
        res.notFound = function(message, code){};
        res.badRequest = function(error){};
        res.result = function(data,meta){
            var response = {
                data:data
            };
            if (meta){
                response.meta = metadata;
            }
            res.json(response);
        };
        next();
    }
}