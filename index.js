const express = require('express')
const morgan = require('morgan')
const path = require('path')
const {join} = require('path')
const api = require('./routes/api.js')
const web = require('./routes/web.js')

const app = express()
const port = 3000

//Load static files
app.use(express.static(__dirname + '/public'))

//Set view engine
app.set('view engine','ejs')

//Third Party Middleware for logging
app.use(morgan('dev'))

//load routes
app.use('/api',api)
app.use('/',web)

//Internal server error handler middleware 
app.use(function(err,req,res,next){
    res.status(500).json({
        status:'fail',
        errors: err.message
    })
})

//404 handlew middleware
app.use(function(req,res,next){
    res.render(join(__dirname,'./views/404.ejs'))
})

//run
app.listen(port,()=> {
    console.log('berhasil dijalankan')
})